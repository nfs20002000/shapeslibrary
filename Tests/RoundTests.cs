using ShapesLibrary;
using ShapesLibrary.Shapes;

namespace Tests
{
    public class RoundTests
    {
        [Fact]
        public void Ctor_Default_ShouldCreateDefaultRound()
        {
            Round round = new Round();

            Assert.NotNull(round);
            Assert.Equal(1, round.Radius);
        }

        [Theory]
        [InlineData(-10)]
        [InlineData(-10.2)]
        [InlineData(-1000)]
        [InlineData(-0xFF)]
        [InlineData(-0b1000001)]
        [InlineData(-10.7f)]
        [InlineData(0)]
        public void Ctor_NonPositiveRadius_ShouldThrowException(double radius)
        {
            Action action = () => new Round(radius);

            Assert.Throws<InvalidLengthException>(action);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(10.2)]
        [InlineData(1000)]
        [InlineData(0xFF)]
        [InlineData(0b1000001)]
        [InlineData(10.7f)]
        public void Ctor_Parametrized_ShouldCreateRound(double radius)
        {
            Round round = new Round(radius);

            Assert.NotNull(round);
            Assert.Equal(radius, round.Radius);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(10.2)]
        [InlineData(1000)]
        [InlineData(0xFF)]
        [InlineData(0b1000001)]
        [InlineData(10.7f)]
        public void GetSquare_ForRadius_ShouldBeValid(double radius)
        {
            Round round = new Round(radius);

            double expected = Math.PI * Math.Pow(radius, 2);
            double actual = round.GetSquare();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ctor_RoundAsShape_ShouldCreateDefaultRound()
        {
            Shape round = new Round();

            bool expected = true;
            bool actual = round is Shape;

            Assert.NotNull(round);
            Assert.Equal(expected, actual);
        }
    }
}