﻿using ShapesLibrary;
using ShapesLibrary.Shapes;

namespace Tests
{
    public class TriangleTests
    {
        [Fact]
        public void Ctor_Default_ShouldCreateDefaultTriangle()
        {
            Triangle triangle = new Triangle();

            Assert.NotNull(triangle);
            Assert.Equal(5, triangle.FirstSide);
            Assert.Equal(6, triangle.SecondSide);
            Assert.Equal(7, triangle.ThirdSide);
        }

        [Theory]
        [InlineData(-10, -11, -12)]
        [InlineData(-10.2, -11.2, -12.2)]
        [InlineData(-1000, -1500, 2000)]
        [InlineData(-0xFF, 0xF1, 0xF2)]
        [InlineData(0b1000001, 0b1000001, -0b1000001)]
        [InlineData(10.7f, -10.7f, 10.7f)]
        [InlineData(0, 0, 0)]
        [InlineData(0b1000001, 0, -0b1000001)]
        [InlineData(0, -10.7f, 10.7f)]
        public void Ctor_NonPositiveSides_ShouldThrowException(double firstSide, double secondSide, double thirdSide)
        {
            Action action = () => new Triangle(firstSide, secondSide, thirdSide);

            Assert.Throws<InvalidLengthException>(action);
        }

        [Theory]
        [InlineData(10, 11, 12)]
        [InlineData(10.2, 11.2, 12.2)]
        [InlineData(1000, 1500, 2000)]
        [InlineData(0xFF, 0xF1, 0xF2)]
        [InlineData(0b1000001, 0b1000001, 0b1000001)]
        [InlineData(10.7f, 10.7f, 10.7f)]
        public void Ctor_Parametrized_ShouldCreateTriangle(double firstSide, double secondSide, double thirdSide)
        {
            Triangle triangle = new Triangle(firstSide, secondSide, thirdSide);

            Assert.NotNull(triangle);
            Assert.Equal(firstSide, triangle.FirstSide);
            Assert.Equal(secondSide, triangle.SecondSide);
            Assert.Equal(thirdSide, triangle.ThirdSide);
        }

        [Theory]
        [InlineData(10, 11, 12)]
        [InlineData(10.2, 11.2, 12.2)]
        [InlineData(1000, 1500, 2000)]
        [InlineData(0xFF, 0xF1, 0xF2)]
        [InlineData(0b1000001, 0b1000001, 0b1000001)]
        [InlineData(10.7f, 10.7f, 10.7f)]
        public void HalfMeter_ThreeSides_ShouldReturnValidHalfMeter(double firstSide, double secondSide, double thirdSide)
        {
            Triangle triangle = new Triangle(firstSide, secondSide, thirdSide);

            double expected = (triangle.FirstSide + triangle.SecondSide + triangle.ThirdSide) / 2;
            double actual = triangle.HalfMeter();

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(10, 11, 12)]
        [InlineData(10.2, 11.2, 12.2)]
        [InlineData(1000, 1500, 2000)]
        [InlineData(0xFF, 0xF1, 0xF2)]
        [InlineData(0b1000001, 0b1000001, 0b1000001)]
        [InlineData(10.7f, 10.7f, 10.7f)]
        public void GetSquare_ThreeSides_ShouldReturnValidSquare(double firstSide, double secondSide, double thirdSide)
        {
            Triangle triangle = new Triangle(firstSide, secondSide, thirdSide);
            double halfMeter = triangle.HalfMeter();

            double expected = Math.Sqrt(halfMeter * (halfMeter - triangle.FirstSide) * (halfMeter - triangle.SecondSide) * (halfMeter - triangle.ThirdSide));
            double actual = triangle.GetSquare();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ctor_TriangleAsShape_ShouldCreateDefaultObject()
        {
            Shape triangle = new Triangle();

            bool expected = true;
            bool actual = triangle is Shape;

            Assert.NotNull(triangle);
            Assert.Equal(expected, actual);
        }
    }
}