﻿using ShapesLibrary;
using ShapesLibrary.Shapes;
using System;

namespace SampleCustomerProject
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // basic possibilities
            Shape round = new Round(5);
            Shape triangle = new Triangle(5, 6, 7);

            Round badRound = new Round();
            Triangle badTriangle = new Triangle(4, 5, 6);

            Triangle rightTriangle = new Triangle(3, 4, 5);

            // exceptions handling
            try
            {
                badRound.Radius = -6;
            }
            catch (InvalidLengthException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                badTriangle.ThirdSide = -4;
            }
            catch (InvalidLengthException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Triangle invalidateSidesTriangle = new Triangle(400, 5, 6);
            }
            catch (InvalidLengthException e)
            {
                Console.WriteLine(e.Message);
            }

            Shape[] shapeArr = { round, triangle, badRound, badTriangle, rightTriangle };

            foreach (var shape in shapeArr)
            {
                Console.WriteLine($"The square is {shape.GetSquare()}");
            }

            // dynamic stuff
            dynamic dynamicShape = new Round(7.87);
            Console.WriteLine(dynamicShape);
            Console.WriteLine($"The square is {dynamicShape.GetSquare()}");

            dynamicShape = new Triangle(5.6, 6.3, 7.2);
            Console.WriteLine(dynamicShape);
            Console.WriteLine($"The square is {dynamicShape.GetSquare()}");

            Console.ReadLine();
        }
    }
}
