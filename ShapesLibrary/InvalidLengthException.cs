﻿using System;

namespace ShapesLibrary
{
    public class InvalidLengthException : ApplicationException
    {
        private readonly string messageDetails = string.Empty;

        public override string Message => $"Invalid length. See details: {messageDetails}";
        public InvalidLengthException()
        {

        }

        public InvalidLengthException(string messageDetails)
        {
            this.messageDetails = messageDetails;
        }
    }
}
