﻿namespace ShapesLibrary
{
    public abstract class Shape
    {
        public abstract double GetSquare();

        public override string ToString()
        {
            return "This is the shape object. ";
        }
    }
}