﻿using System;

namespace ShapesLibrary.Shapes
{
    public class Round : Shape
    {
        private double radius;
        public double Radius
        {
            get => radius;
            set
            {
                if (value <= 0)
                {
                    radius = 1;
                    throw new InvalidLengthException($"Radius must be positive number! Value was: {value}. " +
                        $"Set standard value (1) for radius.");
                }

                radius = value;
            }
        }

        public Round()
        {
            Radius = 1;
        }

        public Round(double radius)
        {
            Radius = radius;
        }

        public override double GetSquare()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }

        public override string ToString()
        {
            return base.ToString() + $"The round with the radius {Radius}";
        }
    }
}
