﻿using System;

namespace ShapesLibrary.Shapes
{
    public class Triangle : Shape
    {
        private double firstSide;
        public double FirstSide { get => firstSide; set
            {
                if (value <= 0)
                {
                    firstSide = (ThirdSide + SecondSide) / 2;
                    throw new InvalidLengthException($"Side must be positive number! Value was: {value}. " +
                        $"Set standard value {firstSide} (half sum of other sides) for firstSide.");
                }

                firstSide = value;
            }
        }

        private double secondSide;
        public double SecondSide { get => secondSide; set
            {
                if (value <= 0)
                {
                    secondSide = (FirstSide + ThirdSide) / 2;
                    throw new InvalidLengthException($"Side must be positive number! Value was: {value}. " +
                        $"Set standard value {secondSide} (half sum of other sides) for secondSide.");
                }

                secondSide = value;
            }
        }

        private double thirdSide;
        public double ThirdSide { get => thirdSide; set
            {
                if (value <= 0)
                {
                    thirdSide = (FirstSide + SecondSide) / 2;
                    throw new InvalidLengthException($"Side must be positive number! Value was: {value}. " +
                        $"Set standard value {thirdSide} (half sum of other sides) for thirdSide.");
                }

                thirdSide = value;
            }
        }

        public Triangle()
        {
            FirstSide = 5;
            SecondSide = 6;
            ThirdSide = 7;
        }

        public Triangle(double firstSide, double secondSide, double thirdSide)
        {
            if (IsValidTriangle(firstSide, secondSide, thirdSide))
            {
                FirstSide = firstSide;
                SecondSide = secondSide;
                ThirdSide = thirdSide;
            }
            else
            {
                FirstSide = 5;
                SecondSide = 6;
                ThirdSide = 7;
                throw new InvalidLengthException("The triangle inequality was not observed." +
                    " Set standard values (5, 6, 7) for the sides.");
            }
        }
        public override double GetSquare()
        {
            if (IsRight())
            {
                return GetSquareForRightTriangle();
            }

            double halfMeter = HalfMeter();
            return Math.Sqrt(halfMeter * (halfMeter - FirstSide) * (halfMeter - SecondSide) * (halfMeter - ThirdSide));
        }

        public double HalfMeter()
        {
            return (FirstSide + SecondSide + ThirdSide) / 2;
        }

        public override string ToString()
        {
            return base.ToString() + $"The triangle with the sides {FirstSide}, {SecondSide}, {ThirdSide}";
        }

        public bool IsValidTriangle(double firstSide, double secondSide, double thirdSide)
        {
            return firstSide < secondSide + thirdSide && secondSide < firstSide + thirdSide && thirdSide < secondSide + firstSide;
        }

        public bool IsRight()
        {
            return Math.Pow(FirstSide, 2) == Math.Pow(SecondSide, 2) + Math.Pow(ThirdSide, 2) ||
                Math.Pow(SecondSide, 2) == Math.Pow(FirstSide, 2) + Math.Pow(ThirdSide, 2) ||
                Math.Pow(ThirdSide, 2) == Math.Pow(SecondSide, 2) + Math.Pow(FirstSide, 2);
        }

        private double GetSquareForRightTriangle()
        {
            if (FirstSide > SecondSide && FirstSide > ThirdSide)
            {
                return SecondSide * ThirdSide / 2;
            }
            else if (SecondSide > FirstSide && SecondSide > ThirdSide)
            {
                return FirstSide * ThirdSide / 2;
            }
            else
            {
                return FirstSide * SecondSide / 2;
            }
        }
    }
}
