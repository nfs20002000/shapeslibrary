USE [master]
GO
/****** Object:  Database [TestTaskForMindbox]    Script Date: 28.09.2022 21:57:43 ******/
CREATE DATABASE [TestTaskForMindbox]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TestTaskForMindbox', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLSERVER2013\MSSQL\DATA\TestTaskForMindbox.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TestTaskForMindbox_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLSERVER2013\MSSQL\DATA\TestTaskForMindbox_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TestTaskForMindbox] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestTaskForMindbox].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestTaskForMindbox] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [TestTaskForMindbox] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestTaskForMindbox] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestTaskForMindbox] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TestTaskForMindbox] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestTaskForMindbox] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TestTaskForMindbox] SET  MULTI_USER 
GO
ALTER DATABASE [TestTaskForMindbox] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestTaskForMindbox] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestTaskForMindbox] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestTaskForMindbox] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [TestTaskForMindbox]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 28.09.2022 21:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 28.09.2022 21:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Price] [float] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product_Category]    Script Date: 28.09.2022 21:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Category](
	[ProductId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([Id], [Name]) VALUES (1, N'Utensil')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (2, N'Book')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (3, N'Phone')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (4, N'Antique')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (1, N'War and peace', 500, 10)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (2, N'My system', 700, 60)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (3, N'A brief history of time', 1000, 50)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (5, N'Fork', 100, 1000)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (6, N'Spoon', 120, 300)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (7, N'Pot', 300, 1000)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (8, N'Cup', 250, 2000)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (9, N'Jug', 300, 1000)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (10, N'Android', 10000, 1000)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (11, N'IPhone', 70000, 5000)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (12, N'Lenovo', 20000, 3000)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (13, N'Lonely product without category', 1, 1)
INSERT [dbo].[Product] ([Id], [Name], [Price], [Quantity]) VALUES (14, N'Unique golden spoon', 100000, 1)
SET IDENTITY_INSERT [dbo].[Product] OFF
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (1, 2)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (2, 2)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (3, 2)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (5, 1)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (6, 1)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (7, 1)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (8, 1)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (9, 1)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (10, 3)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (11, 3)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (12, 3)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (14, 1)
INSERT [dbo].[Product_Category] ([ProductId], [CategoryId]) VALUES (14, 4)
/****** Object:  Index [IX_Product_Category]    Script Date: 28.09.2022 21:57:43 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Product_Category] ON [dbo].[Product_Category]
(
	[ProductId] ASC,
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Category2] FOREIGN KEY([Id])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Category] CHECK CONSTRAINT [FK_Category_Category2]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Product] FOREIGN KEY([Id])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Product]
GO
USE [master]
GO
ALTER DATABASE [TestTaskForMindbox] SET  READ_WRITE 
GO
