SELECT Product.Name, Category.Name
FROM Product as product
LEFT JOIN Product_Category
	on product.Id = Product_Category.ProductId
LEFT JOIN Category as category
	on category.Id = Product_Category.CategoryId