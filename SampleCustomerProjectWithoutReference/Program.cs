﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SampleCustomerProjectWithoutReference
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Assembly asm = Assembly.Load("ShapesLibrary");

                Type roundType = asm.GetType("ShapesLibrary.Shapes.Round");
                Type triangleType = asm.GetType("ShapesLibrary.Shapes.Triangle");

                dynamic round = Activator.CreateInstance(roundType, new object[] { 5 });
                dynamic triangle = Activator.CreateInstance(triangleType, new object[] { 5.8, 4.6, 8.9 });

                double roundSquare = round.GetSquare();
                double triangleSquare = triangle.GetSquare();

                Console.WriteLine($"{round}. The square is {roundSquare}");
                Console.WriteLine($"{triangle}. The square is {triangleSquare}");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
